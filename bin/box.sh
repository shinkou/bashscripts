#!/bin/bash
# vim: ff=unix fileencoding=utf-8 lcs=tab\:>. list noet sw=4 ts=4 tw=0
function show_help()
{
	cat <<EOF
Usage: $0 [ OPTION ] MSG [ MSG ] ...

Option:
  -w WIDTH  Width of the boxes

EOF
}

function drawbox()
{
	msg="$1"
	len=$2
	if [[ -z $len ]]; then
		len=${#msg}
	fi
	echo -n '┌'
	for ((i=0;i<$len;i++)); do
		echo -n '─'
	done
	echo '┐'
	echo -n '│'
	if [[ $len -ge ${#msg} ]]; then
		printf "%-${len}s" "$msg"
	else
		echo -n "${msg::$len}"
	fi
	echo '│'
	echo -n '└'
	for ((i=0;i<$len;i++)); do
		echo -n '─'
	done
	echo '┘'
}

while getopts 'hw:' o
do
	case "$o" in
		h) show_help; shift;;
		w) boxwidth=$OPTARG; shift; shift;;
		?) show_help; exit 1;;
	esac
done

if [[ -n $boxwidth ]]; then
	for s in "$@"; do drawbox "$s" $boxwidth; done
else
	for s in "$@"; do drawbox "$s"; done
fi
